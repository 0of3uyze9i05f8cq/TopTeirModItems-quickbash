#!/bin/bash

#   Script should be run from the following path:
#   '$XDG_DATA_HOME'/binding of isaac afterbirth+ mods/

#   This script goes through all folders one directory deep and deletes  the
#   "disable.it" file, enabling all mods.

#   Copyright (C) 2017 0of3uyze9i05f8cq
#   
#   This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

echo Enabling all mods... USE WITH CAUTION
find . -mindepth 1 -maxdepth 1 -type d -exec rm disable.it {} \;
echo All mods have been enabled. EnableMods boolean has not been changed. Don\'t blame me if you game crashed because you forgot about a broken mod.

# Uses absolute path, while the game follows XDG base directory specification. Unless you have changed this or are using a weird distro this will work, but you should have some idea already. Not sure why it doesn't work. awk is hard imo.

# Not sure why this awk command doesn't work. It should replace the EnableMods boolean from the options.ini. Doesn't really matter since Isaac doesn't care about mods being enabled when there are none loaded.
#awk -v lines=17 'BEGIN { mark = -1 } /pattern/ { mark = NR + lines } NR == mark { sub("EnableMods=0", "EnableMods=1") } { print }' '~/.local/share/binding of isaac afterbirth+/options.ini'
