#!/bin/bash

#   Script should be run from the following path:
#   '$XDG_DATA_HOME'/binding of isaac afterbirth+ mods/

#   This script disables all mods in the collection by checking for the
#   folder name and creating a 'disable.it' file. It displays the name
#   of the mod that the game parses from the xml.

#   Copyright (C) 2017 0of3uyze9i05f8cq
#   
#   This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

echo +-+ DISABLING Mods within the Collection "Top Teir Mod Items" by HiHowAreYou... +-+
echo +-+ Note that if you automatically subscribe to mods in the collection, this script is likely out of date. +-+
# The Collection is here: http://steamcommunity.com/sharedfiles/filedetails/?id=882682026
# HiHowAreYou's workshop is here: http://steamcommunity.com/id/mattdchilds/myworkshopfiles/?appid=250900

# rain boots_903215496/
mod=(./rain boots_903215496/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# Isaac's Dojo
mod=(./isaac\'s\ dojo_892243849)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# Lil' Mole
mod=(./lil mole_854621789)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# Moving Box
mod=(./movingbox_858673093)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# tesla head
mod=(./tesla head_873769758/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# d15_866955594
mod=(./d15_866955594/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# box cutter_835115052
mod=(./box cutter_835115052/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# bighorns hand_836009548
mod=(./bighorns hand_836009548/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# portaloffriends_838222609
mod=(./portaloffriends_838222609/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# rockbomb_838575790
mod=(./rockbomb_838575790/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# broken2020_838845308
mod=(./broken2020_838845308/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# dicetear_838951367
mod=(./dicetear_838951367/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# liliumdimension_846186932
mod=(./liliumdimension_846186932/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# jumper cables_846644717
mod=(./jumper cables_846644717/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# keysack_847040965
mod=(./keysack_847040965/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# cereal cutout_839401874
mod=(./cereal cutout_839401874/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# golden idol_841611185
mod=(./golden idol_841611185/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# beemod_843418469
mod=(./beemod_843418469/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# cystinosis_848118566
mod=(./cystinosis_848118566/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# noose of the keeper_849153461
mod=(./noose of the keeper_849153461/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# imaginaryfriends_850054205
mod=(./imaginaryfriends_850054205/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# headlesshorseman_851903146
mod=(./headlesshorseman_851903146/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# littlest horn_852346005
mod=(./littlest horn_852346005/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# lilim_853198712
mod=(./lilim_853198712/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# lost\ plushie\ \(item\)_855368980
mod=(./lost plushie \(item\)_855368980/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# tearsword_857006260
mod=(./tearsword_857006260/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# weldingmask_858499203
mod=(./weldingmask_858499203/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# bankshot_891893639
mod=(./bankshot_891893639/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# pothead_859698564
mod=(./pothead_859698564/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# d666_866957350
mod=(./d666_866957350/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
#xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xmlecho -n Disabled \" 
echo \".

# occult_siblings_867547951
mod=(./occult_siblings_867547951/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# withdrawal_867865200
mod=(./withdrawal_867865200/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# rainbow fury_873384070
mod=(./rainbow fury_873384070/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# bluebandana_875165460
mod=(./bluebandana_875165460/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# cannibals!_884911055
mod=(./cannibals!_884911055/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# use\ \& care guide_855972287
mod=(./use\ \&\ care\ guide_855972287/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# marshmallow_840727122
mod=(./marshmallow_840727122/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# Technology Zero - directory: shocktears_850840324
mod=(./shocktears_850840324/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# ascalonspear_843808390
mod=(./ascalonspear_843808390/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# lil harbingers_848969396
mod=(./lil harbingers_848969396/)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# thought_869439958
mod=(./thought_869439958)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

# happy_pack_880006673
mod=(./happy_pack_880006673)
touch "${mod[*]}"/disable.it
echo -n Disabled \" 
xmllint --xpath "string(//name[1])" "${mod[*]}"/metadata.xml
echo \".

echo +-+                                 Done                                         +-+
