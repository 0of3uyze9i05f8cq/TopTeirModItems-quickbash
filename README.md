Some bash scripts to enable and disable the mods in the Collection: "Top Tier Mod Items" / all mods.

Script should be run from the following path:

'$XDG_DATA_HOME'/binding of isaac afterbirth+ mods/


Which by default on most GNU/Linux distros is:

~/.local/share/binding of isaac afterbirth+ mods/


I think on MacOS the static path is:

~/Library/Application Support/Binding of Isaac Afterbirth+ mods/


I think on Windows the static path is:

%USERPROFILE%\Documents\My Games\Binding of Isaac Afterbirth+ mods/

