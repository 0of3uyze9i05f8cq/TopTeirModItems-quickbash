#!/bin/bash

#   Script should be run from the following path:
#   '$XDG_DATA_HOME'/binding of isaac afterbirth+ mods/

#   This script goes through all folders one directory deep and creates a
#   "disable.it" file, disabling all mods.

#   Copyright (C) 2017 0of3uyze9i05f8cq
#   
#   This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

echo Disabling all mods...
> disable.it
find . -mindepth 1 -maxdepth 1 -type d -exec cp disable.it {} \;
rm disable.it
#find . -mindepth 1 -maxdepth 1 -type d -exec touch disable.it {} \;
echo All mods have been disabled. EnableMods boolean has not been changed, you can change it in 

# Uses absolute path, while the game follows XDG base directory specification. Unless you have changed this or are using a weird distro this will work, but you should have some idea already. Not sure why it doesn't work. awk is hard imo.
